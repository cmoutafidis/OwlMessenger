# Owl Messenger

Owl Messenger is a Minecraft plugin that allows players to communicate with each other, sending letters or packages to each other.<br/>
This plugin was created to be added to a Harry Potter theme server, this is why the messages are make references to wizards and teachers!

## Dependencies

  - Citizens Plugin
  - Jackson (annotation, core, databind)
  - Json

## Introduction

Owl Messenger allows players to send letters and packages to eachother. They can access them anytime they want from an Owl npc.<br/>
Runing the plugin to your server the first time will generate a config file that contains your database information. Stop the server and re-run it with your updated credentials!

## Commands

  - `/spawnowl` Is used to spawn an owl messenger. Requires `owlmessenger.create` permission.
  - `/sendletter [receiver] [message]` Is used to send a message to another player. Requires `owlmessenger.send` permission.
  - `/sendpackage [receiver]` Is used to send the item at hand to another player. Requires `owlmessenger.send` permission.
  
### Todos

  - Add config file for the messages that are send to the players.
  - Ability to also send a letter with the package so when a player recieves his package, he will get a message in the chat.
  - Add any other cool idea or improvement you guys suggest!
package com.test;

import com.test.owl.commands.OwlSpawner;
import com.test.owl.commands.OwlTransferPackage;
import com.test.owl.commands.OwlTransferLetter;
import com.test.owl.listeners.OwlInteract;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.*;
import java.sql.*;
import java.util.Properties;

public class OwlMessenger extends JavaPlugin {

    private Connection connection;
    private String host, database, username, password;
    private int port;
    private Properties prop = new Properties();
    private InputStream input = null;
    private OutputStream output = null;

    @Override
    public void onEnable(){
        this.getCommand("spawnowl").setExecutor(new OwlSpawner());
        this.getCommand("sendletter").setExecutor(new OwlTransferLetter(this));
        this.getCommand("sendpackage").setExecutor(new OwlTransferPackage(this));
        new OwlInteract(this);

        try {
            File f = new File("plugins/OwlMessenger/config.properties");
            if(!f.exists() || f.isDirectory()) {
                new File("plugins/OwlMessenger").mkdirs();
                this.output = new FileOutputStream("plugins/OwlMessenger/config.properties");
                prop.setProperty("host", "localhost");
                prop.setProperty("port", "3306");
                prop.setProperty("database", "minecraft");
                prop.setProperty("username", "root");
                prop.setProperty("password", "");
                try {
                    prop.store(output, null);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            input = new FileInputStream("plugins/OwlMessenger/config.properties");
            try {
                prop.load(input);
            } catch (IOException e) {
                e.printStackTrace();
            }
            this.host = prop.getProperty("host");
            this.port = Integer.parseInt(prop.getProperty("port"));
            this.database = prop.getProperty("database");
            this.username = prop.getProperty("username");
            this.password = prop.getProperty("password");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            this.openConnection();
            Statement statement = this.connection.createStatement();

            try{
                ResultSet result = statement.executeQuery("SELECT * FROM owlmessenger WHERE id = 1");
            } catch (Exception e){
                statement.executeUpdate("CREATE TABLE owlmessenger (\n" +
                        "  id INT NOT NULL AUTO_INCREMENT,\n" +
                        "  sender VARCHAR(36) NOT NULL,\n" +
                        "  receiver VARCHAR(36) NOT NULL,\n" +
                        "  item VARCHAR(2000),\n" +
                        "  message VARCHAR(2000),\n" +
                        "  PRIMARY KEY (`id`)\n" +
                        ");");

                statement.executeUpdate("CREATE INDEX sender_index on owlmessenger (receiver);");
            }
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }


    }

    @Override
    public void onDisable(){
    }

    public void openConnection() throws SQLException, ClassNotFoundException {
        if (connection != null && !connection.isClosed()) {
            return;
        }

        synchronized (this) {
            if (connection != null && !connection.isClosed()) {
                return;
            }
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://" + this.host+ ":" + this.port + "/" + this.database, this.username, this.password);
        }
    }

    public Connection getConnection() {
        return connection;
    }
}

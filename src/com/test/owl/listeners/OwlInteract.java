package com.test.owl.listeners;

import com.test.OwlMessenger;
import com.test.owl.gui.IconMenu;
import com.test.owl.helpers.JsonItemStack;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.IOException;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class OwlInteract implements Listener {

    private HashMap<String, IconMenu> menu;
    private HashMap<String, ArrayList<String[]>> items;
    private HashMap<String, ArrayList<String[]>> messages;
    private OwlMessenger instance;
    private String loreFormat;
    private String titleFormat;

    public OwlInteract(OwlMessenger plugin) {
        plugin.getServer().getPluginManager().registerEvents(this, plugin);
        this.instance = plugin;
        this.loreFormat = ChatColor.RESET + "" + ChatColor.GRAY;
        this.titleFormat = ChatColor.RESET + "" + ChatColor.GOLD;
        this.menu = new HashMap<>();
        this.items = new HashMap<>();
        this.messages = new HashMap<>();
    }

    @EventHandler
    public void onRightClick(NPCRightClickEvent e) {

        if(e.getNPC().getEntity().getType() == EntityType.BAT && e.getNPC().getName().equals("Owl Messenger")) {
            if(e.getClicker().hasPermission("owlmessenger.interact.open")){
                this.generateMainMenu(e.getClicker().getUniqueId().toString());
                this.menu.get(e.getClicker().getUniqueId().toString()).open(e.getClicker());
            }
            else{
                e.getClicker().sendMessage(ChatColor.GOLD + "You will be able to interact with an owl in the next school years.");
            }
        }
    }

    private void generateMainMenu(String UUid){
        if(this.menu.get(UUid) != null){
            this.menu.get(UUid).destroy();
        }
        this.menu.put(UUid, new IconMenu("Owl Messenger", 45, event -> {
            event.setWillClose(true);
            switch (ChatColor.stripColor(event.getName())){
                case "Letters":
                    this.generateLettersMenu(UUid);
                    break;
                case "Packages":
                    this.generatePackagesMenu(UUid);
                    break;
            }
            return this.menu.get(UUid);
        }, this.instance));

        int countLetters = 0;
        int countPackages = 0;
        try {
            this.instance.openConnection();
            Connection connection = this.instance.getConnection();
            String query = "SELECT id, sender, message FROM owlmessenger WHERE item IS NULL AND receiver = ? ORDER BY id DESC;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setString(1, UUid);
            ResultSet rs = statement.executeQuery();

            ArrayList<String[]> m = new ArrayList<>();
            while (rs.next()) {
                m.add(new String[]{rs.getString(1), rs.getString(2), rs.getString(3)});
                countLetters ++;
            }
            this.messages.put(UUid, m);

            query = "SELECT id, sender, item FROM owlmessenger WHERE item IS NOT NULL AND receiver = ? ORDER BY id DESC;";
            statement = connection.prepareStatement(query);
            statement.setString(1, UUid);
            rs = statement.executeQuery();

            ArrayList<String[]> p = new ArrayList<>();
            while (rs.next()) {
                p.add(new String[]{rs.getString(1), rs.getString(2), rs.getString(3)});
                countPackages ++;
            }
            this.items.put(UUid, p);

        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        String[] array;
        array = new String[]{this.loreFormat + "Your communication with other wizards.",
                            this.loreFormat + "You currently have " + ChatColor.UNDERLINE + countLetters + this.loreFormat + " letters."};
        this.menu.get(UUid).setOption(20, new ItemStack(Material.PAPER, 1), this.titleFormat + "Letters",
                array);

        array = new String[]{this.loreFormat + "Send and receive gifts", this.loreFormat + "with other wizards.",
                            this.loreFormat + "You currently have " + ChatColor.UNDERLINE + countPackages + this.loreFormat + " packages."};
        this.menu.get(UUid).setOption(24, new ItemStack(Material.CHEST, 1), this.titleFormat + "Packages",
                array);
    }

    private void generateLettersMenu(String UUid){
        if(this.menu.get(UUid) != null){
            this.menu.get(UUid).destroy();
        }
        this.menu.put(UUid, new IconMenu("Owl Messenger", 45, event -> {
            ItemMeta meta = event.getItems().getItemMeta();
            meta.setDisplayName(meta.getDisplayName().split(":")[0]);
            ItemStack item = event.getItems();
            item.setItemMeta(meta);
            event.getPlayer().sendMessage("You received a " + meta.getDisplayName().split(":")[0]);
            event.setWillClose(true);
            event.getPlayer().getInventory().addItem(item);
            removeEntityFromDatabase(Integer.parseInt(event.getName().split(":")[1]));
            return null;
        }, this.instance));

        String[] array;
        int indexCounter = 0;
        for(String[] currentMessage : this.messages.get(UUid)){
            if(indexCounter > 26){
                break;
            }

            array = new String[]{this.loreFormat + currentMessage[2]};
            this.menu.get(UUid).setOption(indexCounter, new ItemStack(Material.PAPER, 1), this.titleFormat + "Letter from " + currentMessage[1] + ":" + currentMessage[0],array);
            indexCounter ++;
        }
    }

    private void generatePackagesMenu(String UUid){
        if(this.menu.get(UUid) != null){
            this.menu.get(UUid).destroy();
        }
        this.menu.put(UUid, new IconMenu("Owl Messenger", 45, event -> {
            ItemMeta meta = event.getItems().getItemMeta();
            String[] displayNameArray = meta.getDisplayName().split(":");
            StringBuilder displayName = new StringBuilder();
            for(int i=0; i<displayNameArray.length - 2; i++){
                displayName.append(displayNameArray[i]);
            }
            meta.setDisplayName(displayName.toString());
            ItemStack item = event.getItems();
            item.setItemMeta(meta);
            event.getPlayer().sendMessage("You received a " + meta.getDisplayName().split(":")[0]);
            event.setWillClose(true);
            event.getPlayer().getInventory().addItem(item);
            removeEntityFromDatabase(Integer.parseInt(displayNameArray[displayNameArray.length-1]));
            return null;
        }, this.instance));

        String[] array;
        int indexCounter = 0;
        for(String[] currentItem : this.items.get(UUid)){
            if(indexCounter > 26){
                break;
            }
            System.out.println(currentItem[2]);
            ItemStack item = JsonItemStack.fromJson(currentItem[2]);
            try{
                List<String> lore = item.getItemMeta().getLore();
                int len = lore.size();
                String[] loreArray = new String[len];
                for(int i=0; i<len; i++){
                    loreArray[i] = lore.get(i);
                }
                this.menu.get(UUid).setOption(indexCounter, item, item.getItemMeta().getDisplayName() + ":" + currentItem[1] + ":" + currentItem[0], loreArray);
                indexCounter ++;
            } catch (Exception e){
                e.printStackTrace();
                Bukkit.getPlayer(UUID.fromString(UUid)).sendMessage(ChatColor.YELLOW + "Looks like there is a problem with the owl courier, please inform your Teacher!");
                return;
            }
        }
    }

    private void removeEntityFromDatabase(int id){
        try {
            this.instance.openConnection();
            Connection connection = this.instance.getConnection();
            String query = "DELETE FROM owlmessenger WHERE id = ?;";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            statement.executeUpdate();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

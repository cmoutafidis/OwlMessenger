package com.test.owl.commands;

import com.test.OwlMessenger;
import com.test.owl.helpers.JsonItemStack;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

public class OwlTransferPackage implements CommandExecutor {

    private OwlMessenger currentPlugin;

    public OwlTransferPackage(OwlMessenger plugin){
        this.currentPlugin = plugin;
    }
    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            return false;
        }

        if(strings.length < 1){
            commandSender.sendMessage(ChatColor.GOLD + "Usage: /sendpackage receiver");
            return false;
        }

        Player receiver = Bukkit.getPlayer(strings[0]);
        if(receiver == null){
            commandSender.sendMessage(ChatColor.GOLD + strings[0] + " is not currently in school.");
            return false;
        }

        ItemStack item = ((Player) commandSender).getItemInHand();
        ((Player)commandSender).setItemInHand(null);
        String itemStackString = String.valueOf(JsonItemStack.toJson(item));

        if(itemStackString == null || item.getType().toString().equals("AIR")){
            commandSender.sendMessage(ChatColor.GOLD + "Please hold the item you want to send in your hand.");
            return false;
        }

        try {
            this.currentPlugin.openConnection();
            Connection connection = this.currentPlugin.getConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT INTO owlmessenger (sender, receiver, item) VALUES(?, ?, ?);");
            statement.setString(1, commandSender.getName());
            statement.setString(2, receiver.getUniqueId().toString());
            statement.setString(3, itemStackString);
            statement.executeUpdate();

            commandSender.sendMessage(ChatColor.GOLD + "You successfully send a package to " + receiver.getName() + ".");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            commandSender.sendMessage(ChatColor.YELLOW + "Looks like there is a problem with the owl courier, please inform your Teacher!");
            return false;
        }
        return true;
    }
}

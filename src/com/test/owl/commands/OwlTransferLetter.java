package com.test.owl.commands;

import com.test.OwlMessenger;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class OwlTransferLetter implements CommandExecutor {

    private OwlMessenger currentPlugin;

    public OwlTransferLetter(OwlMessenger plugin){
        this.currentPlugin = plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {
        if(!(commandSender instanceof Player)){
            return false;
        }

        if(strings.length < 2){
            commandSender.sendMessage(ChatColor.GOLD + "Usage: /sendletter receiver message");
            return false;
        }

        Player receiver = Bukkit.getPlayer(strings[0]);
        if(receiver == null){
            commandSender.sendMessage(ChatColor.GOLD + strings[0] + " is not currently in school.");
            return false;
        }

//        if(receiver.getUniqueId().toString().equals(((Player) commandSender).getUniqueId().toString())){
//            commandSender.sendMessage(ChatColor.GOLD + "You can not send a letter to yourself.");
//            return false;
//        }

        StringBuilder message = new StringBuilder();
        message.append(strings[1]);
        for(int i = 2; i < strings.length; i++){
            message.append(" ").append(strings[i]);
        }

        try {
            this.currentPlugin.openConnection();
            Connection connection = this.currentPlugin.getConnection();

            PreparedStatement statement = connection.prepareStatement("INSERT INTO owlmessenger (sender, receiver, message) VALUES(?, ?, ?);");
            statement.setString(1, commandSender.getName());
            statement.setString(2, receiver.getUniqueId().toString());
            statement.setString(3, message.toString());
            statement.executeUpdate();

            commandSender.sendMessage(ChatColor.GOLD + "You successfully send a letter to " + receiver.getName() + ".");
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            commandSender.sendMessage(ChatColor.YELLOW + "Looks like there is a problem with the owl courier, please inform your Teacher!");
            return false;
        }
        return true;
    }
}
